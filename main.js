/*jslint node:true, vars:true, bitwise:true, unparam:true */
/*jshint unused:true */
// Leave the above lines for propper jshinting
//Type Node.js Here :)
//03b80e5aede84b33a7516ce34ec4c700

var express = require('express');
var app = express();
var server = app.listen(3000, function() {
  console.log('Listening to port %d', server.address().port);
});
var io = require('socket.io').listen(server);

var deviceInformationService;
var midiData;

app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
 
app.use(express.static(__dirname + '/public'));

app.get('/', function(request, response) {
  response.render('index');
});

var noble = require('noble');

noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    noble.startScanning();
    console.log("Scanning...");
  } else {
    noble.stopScanning();
  }
});

noble.on('discover', function(peripheral) {
  //console.log('Found device with local name: ' + peripheral.advertisement.localName);
  //console.log('advertising the following service uuid\'s: ' + peripheral.advertisement.serviceUuids);
  //console.log(typeof(peripheral.advertisement.serviceUuids));
  console.log();
    
  if (peripheral.advertisement.localName == "WBLL") {
    peripheral.connect(function(error) {
      console.log('connected to peripheral: ' + peripheral.advertisement.localName);
      
      peripheral.discoverServices(null, function(error, services) {
        console.log('discovered the following services:');
        for (var i in services) {
          console.log('  ' + i + ' uuid: ' + services[i].uuid);
          console.log(services[i]);
        }
        deviceInformationService = services[2];
        console.log(deviceInformationService);
        deviceInformationService.discoverCharacteristics(null, function(error, characteristics) {           console.log('discovered the following characteristics:');
          for (var i in characteristics) {
            console.log('  ' + i + ' uuid: ' + characteristics[i].uuid);
          }
          var deviceCharacteristics = characteristics[0];
        console.log(deviceCharacteristics);
        deviceCharacteristics.on('read', function(data, isNotify) {
          //deviceCharacteristics.write(new Buffer([0x00, 0x00, 0x00, 0x00, 0x00]), true);
          console.log(data);
          midiData = data;
          //console.log(isNotify);
        });
        //deviceCharacteristics.on('notify', function (data, isNotify) {
          //console.log(data);
        //});                                                                                         
                                                                                                 
        periodicActivity(); 

        function periodicActivity()
        {
          deviceCharacteristics.read();
          //deviceCharacteristics.notify();
          setTimeout(periodicActivity,500); 
        }                                                                                       
        
        });
      });
    });
  }
});
      
      
          
//      peripheral.discoverServices(null, function(error, services) {
//        var deviceInformationService = services[3];
//        console.log(deviceInformationService);
//        console.log('discovered device information service');
//
//        deviceInformationService.discoverCharacteristics(null, function(error, characteristics) {             console.log('discovered the following characteristics:');
//          for (var i in characteristics) {
//            console.log('  ' + i + ' uuid: ' + characteristics[i].uuid);
//          }
//          var manufacturerNameCharacteristic = characteristics[0];
//          console.log(manufacturerNameCharacteristic);
//          manufacturerNameCharacteristic.read(function(error, data) {
//            // data is a buffer
//            console.log('manufacture name is: ' + data);
//          });
//        });
//      });


//接続スタート
io.sockets.on("connection", function(socket) {
  console.log("sio start");
  socket.on("msg", function(data) {
    io.emit('msg', data);
  });
  socket.on('send', function(data) {
    io.emit('push', data);
  });
   
  //接続開始直後
  socket.on('acceptValue', function(data) {
    data[0] = midiData;
    //console.log(data[0]);
    io.emit('sendValue', data);
  });
});

